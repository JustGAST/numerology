from classes.argument_parser import ArgumentParser
from classes.numerology_counter import NumerologyCounter
import json

args = ArgumentParser().parse_args()
numerology_counter = NumerologyCounter()

if args.city_name:
    city_name = args.city_name
    print(numerology_counter.count_city_number(city_name))
else:
    file_handler = open('cities.json')
    cities_json = json.load(file_handler)

    cities_counted = []
    for city_json in cities_json:
        city_json['number'] = numerology_counter.count_city_number(city_json['name'])
        cities_counted.append(city_json)

    file_writer = open('cities_counted.json', 'w')
    json.dump(cities_counted, file_writer)