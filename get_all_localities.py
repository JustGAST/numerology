import requests, json

url = 'http://kladr-api.ru/api.php'
params = {"typeCode": "1", "contentType": "city", "query": ""}

cities = []

for letter_code in range(1040, 1071):
    params['query'] = chr(letter_code)
    data = requests.get(url, params=params)
    cities_json = data.json()
    for city_json in cities_json['result']:
        cities.append({'name': city_json['name'], 'type': city_json['type']})

with open('cities.json', 'w') as file_handler:
    json.dump(cities, file_handler)
