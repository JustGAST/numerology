class NumerologyCounter:
    def count_city_number(self, city_name: str):
        city_name = city_name.upper()

        letters_to_numbers_comparison = {
            1: ('А', 'И', 'С', 'Ъ'),
            2: ('Б', 'Й', 'Т', 'Ы'),
            3: ('В', 'К', 'У', 'Ь'),
            4: ('Г', 'Л', 'Ф', 'Э'),
            5: ('Д', 'М', 'Х', 'Ю'),
            6: ('Е', 'Н', 'Ц', 'Я'),
            7: ('Ё', 'О', 'Ч'),
            8: ('Ж', 'П', 'Ш'),
            9: ('З', 'Р', 'Щ')
        }

        letters_sum = 0
        for letter in list(city_name):
            for number, letters_tuple in letters_to_numbers_comparison.items():
                if letter in letters_tuple:
                    letters_sum += number

        while (letters_sum > 9):
            letters_sum = sum([int(number) for number in list(str(letters_sum))])

        return letters_sum
