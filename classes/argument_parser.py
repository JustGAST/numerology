import argparse


class ArgumentParser:
    def parse_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--city_name',
                            help="Город, число которого нужно посчитать",
                            type=str)

        return parser.parse_args()
